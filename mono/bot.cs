using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

public class Bot
{
    public static void Main(string[] args)
    {
        args = "testserver.helloworldopen.com 8091 potato cxrEBXEQGQXNBQ".Split();

        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

        Console.WriteLine(String.Format("Connecting to {0}:{1} as {2}/{3}", host, port, botName, botKey));

        using (var client = new TcpClient(host, port))
        {
            var stream = client.GetStream();
            var reader = new StreamReader(stream);
            var writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            new Bot(reader, writer, new Join(botName, botKey));
        }
    }

    //control coefficient
    const double T_angle = 20;
    const double T_throttle = 0.75;

    const double Kp = 0.01;
    const double Ki = -0.002;
    const double Kd = -0.003;

    double weightedMean = 0.0;
    double Kw = 0.90;

    private StreamWriter writer;
    private Car myCar;
    private Track track;
    private RaceSession raceSession;

    private double throttle;
    private double prevTime;
    private CarPosition prevPosition;

    Bot(StreamReader reader, StreamWriter writer, Join join)
    {
        this.writer = writer;
        string line;

        send(join);

        while ((line = reader.ReadLine()) != null)
        {
            var jobj = JObject.Parse(line);
            MsgWrapper msg = jobj.ToObject<MsgWrapper>();

            //Console.WriteLine("raw : {0}", line);
            switch (msg.msgType)
            {
                case "carPositions":
                    double time = 0;
                    if (jobj["gameTick"] != null) time = (double)jobj["gameTick"];
                    onCarPositions(msg, time);
                    break;
                case "join":
                    Console.WriteLine("Joined");
                    myCar = (msg.data as JObject).ToObject<Car>();
                    send(new Ping());
                    break;
                case "gameInit":
                    Console.WriteLine("Race init");
                    OnGameInit(msg);
                    break;
                case "gameEnd":
                    Console.WriteLine("Race ended");
                    send(new Ping());
                    break;
                case "gameStart":
                    Console.WriteLine("Race starts");
                    send(new Ping());
                    break;
                case "crash":
                    Console.WriteLine("crash somebody");
                    break;
                case "spawn":
                    Console.WriteLine("spawn somebody");
                    break;
                case "tournamentEnd":
                    return;
                default:
                    send(new Ping());
                    break;
            }
        }
    }

    private void onCarPositions(MsgWrapper msg,double time)
    {
        var positions = (msg.data as JArray).ToObject<List<CarPosition>>();
        var myPosition = positions.First(car => car.id.name == myCar.name);
        if(prevPosition != null){
            double mileage = track.GetMileage(prevPosition, myPosition);
            double speed = mileage / (time - prevTime);

            if (speed < 0)
            {
                Console.WriteLine("?");
            }

            Console.WriteLine("time : {0}", time);
            Console.WriteLine("current pos : {0}", myPosition);
            Console.WriteLine("speed : {0}", speed);
        }

        if (prevPosition == null)
        {
            throttle = T_throttle;
        }
        else
        {
            double d = myPosition.angle - prevPosition.angle;
            if (track.pieces[myPosition.piecePosition.pieceIndex].angle < 0) d *= -1;
            d = Math.Max(0, d);

            //PID control
            //P
            double p = T_angle - Math.Abs(myPosition.angle);
            throttle += p * Kp;

            //I?
            weightedMean = weightedMean * Kw + d;
            throttle += Ki * weightedMean;

            //D?
            throttle += d * Kd;

            throttle = Math.Min(T_throttle, Math.Max(0, throttle));
        }


        Console.WriteLine("throttle : {0}", throttle);

        send(new Throttle(throttle));

        prevTime = time;
        prevPosition = myPosition;
    }

    private void OnGameInit(MsgWrapper msg)
    {
        GameInit init = new GameInit(msg.data as JObject);
        track = init.track;
        raceSession = init.raceSession;

        send(new Ping());
    }

    private void send(SendMsg msg)
    {
        writer.WriteLine(msg.ToJson());
    }
}

class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

public class Piece
{
    public double length;
    [JsonProperty("switch")]
    public bool _switch;

    public double radius;
    public double angle;

    public bool IsStraight() { return length != 0.0; }

    public double getLength(double distanceFromCenter)
    {
        if (IsStraight()) return length;

        if (angle > 0) return (radius - distanceFromCenter) * angle / 180.0 * Math.PI;
        else return (radius + distanceFromCenter) * -angle / 180.0 * Math.PI;
    }

    public override string ToString()
    {
        var ret = string.Format("centerLength : {0},radius : {1}",getLength(0),radius);
        if(_switch) ret += ", has switch";
        return ret;
    }
}

public class Track
{
    public string id;
    public string name;
    public List<Piece> pieces;
    public List<double> lanes;

    public Track(JObject obj)
    {
        id = (string)obj["id"];
        name = (string)obj["name"];
        pieces = obj["pieces"].Select(_ => _.ToObject<Piece>()).ToList();
        lanes = obj["lanes"].Select(_ => (double)_["distanceFromCenter"]).ToList();
    }

    public double GetMileage(CarPosition prev,CarPosition next){
        double mileage = -prev.piecePosition.inPieceDistance;
        int startIndex = prev.piecePosition.pieceIndex;
        int endIndex = next.piecePosition.pieceIndex;
        if(endIndex < startIndex) endIndex += pieces.Count;
        for (int _index = startIndex; _index < endIndex; _index++)
        {
            int index = _index % pieces.Count;
            mileage += pieces[index].getLength(lanes[next.piecePosition.lane.endLaneIndex]);
        }
        mileage += next.piecePosition.inPieceDistance;

        return mileage;
    }
};

public class RaceSession
{
    public int laps;
    public double maxLapTimeMs;
    public bool quickRace;
}

public class Car{
    public string name;
    public string color;
}

public class GameInit
{
    public Track track;
    public List<Car> cars;
    public RaceSession raceSession;

    public GameInit(JObject obj)
    {
        track = new Track(obj["race"]["track"] as JObject);
        cars = obj["race"]["cars"].Select(_ => (_["id"] as JObject).ToObject<Car>()).ToList();
        raceSession = (obj["race"]["raceSession"] as JObject).ToObject<RaceSession>();
    }
}

public class LanePosition
{
    public int startLaneIndex;
    public int endLaneIndex;
}

public class PiecePisition
{
    public int pieceIndex;
    public double inPieceDistance;
    public LanePosition lane;

    public override string ToString()
    {
        return string.Format("index : {0},dist : {1}", pieceIndex, inPieceDistance);
    }
};

public class CarPosition
{
    public Car id;
    public double angle;
    public PiecePisition piecePosition;
    public int lap;

    public override string ToString()
    {
        return string.Format("lap : {0}, angle : {1}, pos : [{2}]", lap, angle, piecePosition);
    }
}

abstract class SendMsg
{
    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }

    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

class Join : SendMsg
{
    public string name;
    public string key;
    public string color;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
        this.color = "red";
    }

    protected override string MsgType()
    {
        return "join";
    }
}

class Ping : SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

class Throttle : SendMsg
{
    public double value;

    public Throttle(double value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }
}